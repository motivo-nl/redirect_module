<?php

use Motivo\Liberiser\Base\Models\Permission;
use Motivo\Liberiser\Redirects\Models\Redirect;

return [
    'class' => Redirect::class,
    'package_namespace' => 'LiberiserRedirects',
    'required_permission_level' => Permission::ROLE_MANAGER,
];
