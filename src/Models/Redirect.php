<?php

namespace Motivo\Liberiser\Redirects\Models;

use Motivo\Liberiser\Base\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Redirect extends BaseModel
{
    use SoftDeletes;

    public const TYPE_GENERATED = 0;

    public const TYPE_MANUAL = 1;

    protected $table = 'liberiser_redirects';

    protected $fillable = ['from', 'to', 'type'];

    public static function getModuleName(): string
    {
        return 'redirects';
    }

    public static function getComponentFields(?BaseModel $model = null): array
    {
        return [
            [
                'name' => 'from',
                'type' => 'text',
                'label' => ucfirst(trans('LiberiserRedirects::redirect.fields.from')),

            ],
            [
                'name' => 'to',
                'type' => 'text',
                'label' => ucfirst(trans('LiberiserRedirects::redirect.fields.to')),
            ],
        ];
    }

    public static function getComponentColumns(): array
    {
        return [
            [
                'name' => 'from',
                'type' => 'text',
                'label' => ucfirst(trans('LiberiserRedirects::redirect.fields.from')),

            ],
            [
                'name' => 'to',
                'type' => 'text',
                'label' => ucfirst(trans('LiberiserRedirects::redirect.fields.to')),
            ],
            [
                'name' => 'type',
                'label' => ucfirst(trans('LiberiserRedirects::redirect.fields.type')),
                'type' => 'closure',
                'function' => function ($entry) {
                    switch ($entry->type) {
                        case self::TYPE_GENERATED:
                            return ucfirst(trans('LiberiserRedirects::redirect.types.generated'));
                        case self::TYPE_MANUAL:
                            return ucfirst(trans('LiberiserRedirects::redirect.types.manual'));
                        default:
                            return '';
                    }
                },
            ],
        ];
    }

    public static function getComponentRelations(): array
    {
        return [];
    }

    public function getDisplayNameAttribute(): string
    {
        return $this->from.' > '.$this->to;
    }
}
