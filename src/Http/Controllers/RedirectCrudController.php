<?php

namespace Motivo\Liberiser\Redirects\Http\Controllers;

use Motivo\Liberiser\Redirects\Models\Redirect;
use Motivo\Liberiser\Redirects\Http\Requests\StoreRedirectRequest;
use Motivo\Liberiser\Base\Http\Controllers\LiberiserCrudController;
use Motivo\Liberiser\Redirects\Http\Requests\UpdateRedirectRequest;

class RedirectCrudController extends LiberiserCrudController
{
    protected $model = Redirect::class;

    protected $storeRequest = StoreRedirectRequest::class;

    protected $updateRequest = UpdateRedirectRequest::class;

    protected $crudRoute = '/redirect';

    protected $entityNameSingular = 'LiberiserRedirects::redirect.singular';

    protected $entityNamePlural = 'LiberiserRedirects::redirect.plural';

    public function store(StoreRedirectRequest $request)
    {
        return parent::processStore($request);
    }

    public function update(UpdateRedirectRequest $request)
    {
        return parent::processUpdate($request);
    }
}
