<?php

namespace Motivo\Liberiser\Redirects\Http\Requests;

use Illuminate\Support\Facades\Request;
use Motivo\Liberiser\Base\Models\BaseModel;
use Illuminate\Contracts\Validation\Validator;
use Motivo\Liberiser\Redirects\Models\Redirect;
use Motivo\Liberiser\Base\Http\Requests\LiberiserRequest;
use Motivo\Liberiser\Base\Http\Requests\UpdateLiberiserRequest;

class UpdateRedirectRequest extends UpdateLiberiserRequest
{
    protected $model = Redirect::class;

    public function __construct(array $query = [], array $request = [], array $attributes = [], array $cookies = [], array $files = [], array $server = [], $content = null)
    {
        $redirect = Request::route('redirect');

        if (is_int($redirect) || is_string($redirect)) {
            $redirect = Redirect::find($redirect);
        }

        $this->entity = $redirect;

        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
    }

    public function getValidatorInstance(): Validator
    {
        $this->merge(['type' => Redirect::TYPE_MANUAL]);

        return parent::getValidatorInstance();
    }

    public static function getRules(LiberiserRequest $request, ?BaseModel $entity): array
    {
        $id = $entity ? $entity->id : 0;

        return [
            'from' => [
                'required',
                'unique:liberiser_redirects,from,'.$id,
                'regex:/^\//i',
            ],
            'to' => [
                'required',
                'regex:/^\//i',
            ],
            'type' => [
                'required',
                'integer',
            ],
        ];
    }
}
