<?php

namespace Motivo\Liberiser\Redirects\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Motivo\Liberiser\Redirects\Models\Redirect;
use Motivo\Liberiser\Base\Http\Requests\LiberiserRequest;
use Motivo\Liberiser\Base\Http\Requests\StoreLiberiserRequest;

class StoreRedirectRequest extends StoreLiberiserRequest
{
    protected $model = Redirect::class;

    public function getValidatorInstance(): Validator
    {
        $this->merge(['type' => Redirect::TYPE_MANUAL]);

        return parent::getValidatorInstance();
    }

    public static function getRules(LiberiserRequest $request): array
    {
        return [
            'from' => [
                'required',
                'unique:liberiser_redirects,from',
                'regex:/^\//i',
            ],
            'to' => [
                'required',
                'regex:/^\//i',
            ],
            'type' => [
                'required',
                'integer',
            ],
        ];
    }
}
