<?php

namespace Motivo\Liberiser\Redirects;

use Motivo\Liberiser\Base\Liberiser;
use Illuminate\Support\ServiceProvider;
use Motivo\Liberiser\Base\BaseMenuItem;
use Motivo\Liberiser\Redirects\Models\Redirect;

class RedirectsServiceProvider extends ServiceProvider
{
    /** @var string */
    protected $publishablePrefix = 'liberiser';

    /** @var string */
    protected $packageNamespace = 'LiberiserRedirects';

    public function register(): void
    {
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');

        $this->mergeConfigFrom(__DIR__.'/../config/redirects.php', 'liberiser.redirects');
    }

    public function boot(): void
    {
        if ($this->isEnabled()) {
            $this->enabledBoot();
        }
    }

    public function enabledBoot(): void
    {
        $this->registerRoutes();

        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', $this->packageNamespace);

        $this->setMenuItems();

        $this->setModules();
    }

    public function registerRoutes(): void
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/admin.php');
    }

    private function setMenuItems(): void
    {
        if (! $this->app->runningInConsole()) {
            $menu = $this->getBaseMenuItem('fa-exchange', 'LiberiserRedirects::redirect.menu_label', route('crud.redirect.index'), Redirect::getModuleName());

            Liberiser::setAdminMenuItems(BaseMenuItem::CATEGORY_MANAGEMENT, null, $menu);
        }
    }

    private function setModules(): void
    {
        Liberiser::addPageModule(Redirect::getModuleName(), ucfirst(trans('LiberiserRedirects::redirect.menu_label')));
    }

    private function getBaseMenuItem(string $icon, string $label, string $link, string $module): BaseMenuItem
    {
        $menu = new BaseMenuItem();

        $menu->setIcon($icon);
        $menu->setLabel(ucfirst(trans($label)));
        $menu->setLink($link);
        $menu->setModule($module);

        return $menu;
    }

    private function isEnabled(): bool
    {
        $config = Redirect::getConfig();

        if (isset($config['active']) && $config['active'] === false) {
            return false;
        }

        return true;
    }
}
