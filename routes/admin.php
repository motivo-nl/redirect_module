<?php

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'Motivo\Liberiser\Redirects\Http\Controllers',
], function () {
    CRUD::resource('redirect', 'RedirectCrudController');
});
