<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Motivo\Liberiser\Redirects\Models\Redirect;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RedirectsTest extends TestCase
{
    use DatabaseTransactions;
    use WithFaker;

    /** @test */
    public function can_create_redirect_test(): void
    {
        $inputData = [
            'from' => '/'.implode('/', $this->faker->words),
            'to' => '/'.implode('/', $this->faker->words),
        ];

        $response = $this->post(route('crud.redirect.store'), $inputData);

        $response->assertStatus(Response::HTTP_FOUND);

        $this->assertDatabaseHas('liberiser_redirects', $inputData);
    }

    /** @test */
    public function redirects_should_start_with_slash_test(): void
    {
        $inputData1 = [
            'from' => '/'.implode('/', $this->faker->words),
            'to' => implode('/', $this->faker->words),
        ];

        $inputData2 = [
            'from' => implode('/', $this->faker->words),
            'to' => '/'.implode('/', $this->faker->words),
        ];

        $this->post(route('crud.redirect.store'), $inputData1);
        $this->post(route('crud.redirect.store'), $inputData2);

        $this->assertDatabaseMissing('liberiser_redirects', $inputData1);
        $this->assertDatabaseMissing('liberiser_redirects', $inputData2);
    }

    /** @test */
    public function can_edit_redirect_test(): void
    {
        $inputData = [
            'from' => '/'.implode('/', $this->faker->words),
            'to' => '/'.implode('/', $this->faker->words),
        ];

        $updateData = [
            'id' => 1,
            'from' => '/'.implode('/', $this->faker->words),
            'to' => '/'.implode('/', $this->faker->words),
        ];

        $this->post(route('crud.redirect.store'), $inputData)->assertStatus(Response::HTTP_FOUND);
        $this->put(route('crud.redirect.update', ['redirect' => 1]), $updateData)->assertStatus(Response::HTTP_FOUND);

        $this->assertDatabaseMissing('liberiser_redirects', $inputData);
        $this->assertDatabaseHas('liberiser_redirects', $updateData);
    }

    /** @test */
    public function type_for_manual_redirects_is_manual_test(): void
    {
        $inputData = [
            'from' => '/'.implode('/', $this->faker->words),
            'to' => '/'.implode('/', $this->faker->words),
        ];

        $this->post(route('crud.redirect.store'), $inputData)->assertStatus(Response::HTTP_FOUND);

        $this->assertDatabaseHas('liberiser_redirects', array_merge($inputData, ['type' => Redirect::TYPE_MANUAL]));
        $this->assertDatabaseMissing('liberiser_redirects', array_merge($inputData, ['type' => Redirect::TYPE_GENERATED]));
    }

    /** @test */
    public function can_delete_redirect_test(): void
    {
        $inputData = [
            'from' => '/'.implode('/', $this->faker->words),
            'to' => '/'.implode('/', $this->faker->words),
        ];

        $this->post(route('crud.redirect.store'), $inputData)->assertStatus(Response::HTTP_FOUND);
        $this->delete(route('crud.redirect.destroy', ['redirect' => 1]))->assertStatus(Response::HTTP_OK);

        $this->assertSoftDeleted('liberiser_redirects', $inputData);
    }
}
