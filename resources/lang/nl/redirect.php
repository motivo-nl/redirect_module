<?php

return [
    'singular' => 'redirect',
    'plural' => 'redirects',
    'name' => 'redirects',
    'menu_label' => 'redirects',
    'columns' => [
        'from' => 'van',
        'to' => 'naar',
        'type' => 'type',
    ],
    'fields' => [
        'from' => 'van',
        'to' => 'naar',
        'type' => 'type',
    ],
    'types' => [
        'manual' => 'handmatig',
        'generated' => 'automatisch gegenereerd',
    ],
];
