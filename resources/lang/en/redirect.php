<?php

return [
    'singular' => 'redirect',
    'plural' => 'redirects',
    'name' => 'redirects',
    'menu_label' => 'redirects',
    'columns' => [
        'from' => 'from',
        'to' => 'to',
        'type' => 'type',
    ],
    'fields' => [
        'from' => 'from',
        'to' => 'to',
        'type' => 'type',
    ],
    'types' => [
        'manual' => 'manual',
        'generated' => 'automatically generated',
    ],
];
